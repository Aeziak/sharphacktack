﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace sharphacktack.Modules
{
    public class Ping : ModuleBase<SocketCommandContext>
    {
        private bool isTTS = false;
        private string allCmd;
        private List<List<string>> listCmd = new List<List<string>>();

        [Command("ping")]
        public async Task PingAsync()
        {
            await ReplyAsync("Pong !", isTTS);
        }

        [Command("hello")]
        public async Task HelloAsync()
        {
            await ReplyAsync("Hello " + Context.User.Mention, isTTS);
        }

        [Command("del")]
        [Summary("deletes set amount of text")]
        [RequireUserPermission(GuildPermission.Administrator)]
        [RequireBotPermission(GuildPermission.Administrator)]
        public async Task DelAsync(IUserMessage msg)
        {
            int n = 2;
            IReadOnlyCollection < SocketMessage > messages = Context.Channel.GetCachedMessages(n);
            foreach (SocketMessage message in messages)
            {
                await message.DeleteAsync();
            }
            await ReplyAsync(messages.Count + " messages deleted !", isTTS);
        }

        [Command("jostan")]
        public async Task JostanAsync()
        {
            await ReplyAsync("@Aeziak", isTTS);
        }

        [Command("setTTS")]
        public async Task SetTTSAsync()
        {
            isTTS = true;
            await ReplyAsync("TTS is set !", isTTS);
        }

        [Command("unsetTTS")]
        public async Task UnsetTTSAsync()
        {
            isTTS = false;
            await ReplyAsync("TTS is unset !", isTTS);
        }

        [Command("cancer")]
        public async Task CancerAsync()
        {
            await ReplyAsync(Context.Guild.EveryoneRole.Mention, isTTS);
        }

        [Command("test")]
        public async Task TestAsync(IUserMessage msg)
        {
            await ReplyAsync(msg.Content);
        }

        [Command("help")]
        public async Task HelpAsync()
        {
            SetListCmd();
            allCmd += "Here's all the commands : ||```" + Environment.NewLine;
            foreach (List<string> cmd in listCmd)
            {
                int i = 0;
                foreach (string elemCmd in cmd)
                {
                    
                    if (i == 0)
                    {
                        allCmd += elemCmd + " : ";
                    }
                    else
                    {
                        allCmd += "*" + elemCmd + "*" + Environment.NewLine;
                    }
                    i++;
                }
            }
            allCmd += "```||";
            await ReplyAsync(allCmd);
        }

        public void SetListCmd()
        {
            /**
             * Exemple :
             * listCmd.Add(new List<string> { "[Nom de la commande]", "[Description de la commande]" });
             */
            listCmd.Add(new List<string> { "help", "Send all commands !" });
            listCmd.Add(new List<string> { "ping", "Send Hello World !" });
        }
    }
}
