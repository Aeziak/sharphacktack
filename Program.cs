﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace sharphacktack
{
    class Program
    {
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public static void Main(string[] args)
		    => new Program().MainAsync().GetAwaiter().GetResult();

        private System.Threading.Tasks.Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return System.Threading.Tasks.Task.CompletedTask;
        }

        public async System.Threading.Tasks.Task MainAsync()
        {
            _client = new DiscordSocketClient();

            _client.Log += Log;

            _commands = new CommandService();

            _services = new ServiceCollection().AddSingleton(_client).AddSingleton(_commands).BuildServiceProvider();

            // You can assign your bot token to a string, and pass that in to connect.
            // This however is insecure, particularly if you plan to have your code hosted in a repository.
            var token = "NTkyNzczMzc5MTg3Mjc3ODI0.XRENjQ.o6uqGRCUebzEF4JbRZGrQLBVi2A";

            // Some alternative  options would be to keep your token in an Environment Variable or a standalone file.
            // var token = Environment.GetEnvironmentVariable("NameOfYourEnvironmentVariable");
            // var token = File.ReadAllText("token.txt");
            // var token = JsonConvert.DeserializeObject<AConfigurationClass>(File.ReadAllText("config.json")).Token;

            await RegisterCommandAsync();
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            // Block this task until the program is closed.
            await System.Threading.Tasks.Task.Delay(-1);
        }

        public async Task RegisterCommandAsync()
        {
            _client.MessageReceived += HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), null);
        }

        private async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;

            if (message is null || message.Author.IsBot) return;

            int argPos = 0;
            var context = new SocketCommandContext(_client, message);

            Console.WriteLine(message.Timestamp.ToString() + " : " + message.Content);

            if(message.HasStringPrefix("!", ref argPos) || message.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                
                var result = await _commands.ExecuteAsync(context, argPos, _services);

                if(!result.IsSuccess)
                {
                    Console.WriteLine(result.ErrorReason);
                }
            }
            else if(message.Content.Contains("http") || message.Content.Contains("www."))
            {
                await message.DeleteAsync();
            }
        }
    }
}
